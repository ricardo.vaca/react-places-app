import React from "react";

import UsersList from "../components/UsersList";

const Users = () => {
  const USERS = [
    {
      id: "u1",
      name: "Ricardo Vaca",
      image: "https://thisis-images.scdn.co/37i9dQZF1DZ06evO3q11Ek-large.jpg",
      places: 3,
    },
  ];

  return <UsersList items={USERS} />;
};

export default Users;
